package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

        @Test
        public void testMax() throws Exception {
          MinMax minMax = new MinMax();
          int actual = minMax.findMinMax(3,5);
	  int max = 5;
          assertEquals("Max",5,max);
        }

	 @Test
        public void testMin() throws Exception {
          MinMax minMax = new MinMax();
          int actual = minMax.findMinMax(6,5);
          int min = 5;
          assertEquals("Min",5,min);
        }

}

